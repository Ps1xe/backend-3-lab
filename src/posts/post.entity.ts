import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,

} from 'typeorm';

import { User } from '../users/user.entity';

/**
 * Сущность поста делайте сами, опирайтесь на пример User Entity
 */
const tableName = 'posts';

@Entity({ name: tableName })
export class Post {
  /**
   * Не забудьте, что у поста есть автор!
   * Вам нужно связать две сущности
   */
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar')
  content: string;

  @Column('varchar')
  authorId?: string;

  @ManyToOne(() => User, (user) => user.posts)
  author?: User;

  @UpdateDateColumn()
  updatedAt: string;

  @CreateDateColumn()
  createdAt: string;
}
