import { dbConnection } from '../database/database.config';
import { EntityRepository } from '../interfaces';
import { User } from '../users';
import { Post } from './post.entity';
import { CreatePostDto, UpdatePostDto } from './posts.dto';

/**
 * Тут все методы сами описываете, референс с UsersRepository
 */
class PostsRepository implements EntityRepository<Post> {
  async findAll(): Promise<Post[]> {
    try {

      return dbConnection.getRepository(Post).find();

    } catch (e) {
      throw e;
    }
  }

  async findOne(id: string): Promise<Post> {
    try {

      const post = await dbConnection.getRepository(Post).findOne(id);

      if (post === undefined) {
        throw new Error("Пост не найден");
      }
      return post;

    } catch (e) {
      throw e;
    }
  }

  async authorByPostId(id: string): Promise<User> {
    try {

      const post = this.findOne(id);
      return (await post).author!;

    } catch (e) {
      throw e;
    }
  }

  async findByUserId(id: string): Promise<Post[]> {
    try {

      const posts = await dbConnection.getRepository(Post).find({ where: { authorId: id }, relations: ['author'] });
      return posts;

    } catch (e) {
      throw e;
    }
  }

  async createOne(dto: CreatePostDto): Promise<Post> {
    try {

      return dbConnection.getRepository(Post).save(dto);

    } catch (e) {
      throw e;
    }
  }

  async updateOne(id: string, dto: UpdatePostDto): Promise<Post> {
    try {
      return await dbConnection.getRepository(Post).save({
        ...dto,
        id
      });

    } catch (e) {
      throw e;
    }
  }

  async deleteOne(id: string): Promise<boolean> {
    try {

      dbConnection.getRepository(Post).delete(id);
      return true;

    } catch (e) {
      return false;
    }
  }

}

export const postsRepository = new PostsRepository();
