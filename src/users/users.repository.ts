import { dbConnection } from '../database/database.config';
import { EntityRepository } from '../interfaces';
import { Post } from '../posts/post.entity';
import { User } from './user.entity';
import { CreateUserDto, UpdateUserDto } from './users.dto';

/**
 * Допишите оставшиеся методы учитывая пример с findAll
 */
class UsersRepository implements EntityRepository<User> {
  async findAll(): Promise<User[]> {
    try {

      return dbConnection.getRepository(User).find();

    } catch (e) {
      throw e;
    }
  }

  async findOne(id: string): Promise<User> {
    try {

      const user = await dbConnection.getRepository(User).findOne(id);

      if (user === undefined) {
        throw new Error("Пользователь не найден");
      }
      return user;

    } catch (e) {
      throw e;
    }
  }


  async createOne(dto: CreateUserDto): Promise<User> {
    try {

      return dbConnection.getRepository(User).save(dto);

    } catch (e) {
      throw e;
    }
  }

  async updateOne(id: string, dto: UpdateUserDto): Promise<User> {
    try {

      return await dbConnection.getRepository(User).save({
        ...dto,
        id
      });

    } catch (e) {
      throw e;
    }
  }

  async deleteOne(id: string): Promise<boolean> {
    try {

      dbConnection.getRepository(User).delete(id);
      return true;

    } catch (e) {
      return false;
    }
  }
}

export const usersRepository = new UsersRepository();
